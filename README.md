# ds-backend

Backend for the project darce-side.

## Prerequisites

Before you begin, ensure you have met the following requirements:
* You have installed the latest version of Node.js and npm.
* You have a basic understanding of Node.js and Express.js.

## Installing ds-backend

To install ds-backend, follow these steps:

Linux and macOS:

```bash
cd ds-backend
npm install
DEBUG=myapp:* npm start
```

Windows :
```bash
cd ds-backend
npm install
set DEBUG=myapp:* & npm start
```
